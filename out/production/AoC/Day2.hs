module Day2 where

day2 = print $ head (evaluate 0 (replaceNth 1 12 (replaceNth 2 2 input)))
day2' = print $ evaluate' [0,0] input

evaluate index all
  | oppCode == 1 = evaluate (index + 4) (replaceNth output (all!!index1 + all!!index2) all)
  | oppCode == 2 = evaluate (index + 4) (replaceNth output (all!!index1 * all!!index2) all)
  | oppCode == 99 = all
  | otherwise = error "Invalid OppCode"
  where oppCode = all!!index
        index1 = all!!(index+1)
        index2 = all!!(index+2)
        output = all!!(index+3)

evaluate' (noun:verb:_) all
  | result == 19690720 = [100 * noun + verb]
  | otherwise = evaluate' (increaseTuple [noun, verb]) all
  where
    result = head (evaluate 0 (replaceNth 1 noun (replaceNth 2 verb all)))

increaseTuple :: [Int] -> [Int]
increaseTuple (100:99:_) = error "Could not find match"
increaseTuple (99:y:_) = 0 : [y + 1]
increaseTuple (x:y:_) = (x + 1) : [y]

replaceNth _ _ [] = []
replaceNth 0 newVal (x:xs) = newVal:xs
replaceNth n newVal (x:xs) = x:replaceNth (n-1) newVal xs

input = [1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,1,13,19,1,9,19,23,1,6,23,27,2,27,9,31,2,6,31,35,1,5,35,39,1,10,39,43,1,43,13,47,1,47,9,51,1,51,9,55,1,55,9,59,2,9,59,63,2,9,63,67,1,5,67,71,2,13,71,75,1,6,75,79,1,10,79,83,2,6,83,87,1,87,5,91,1,91,9,95,1,95,10,99,2,9,99,103,1,5,103,107,1,5,107,111,2,111,10,115,1,6,115,119,2,10,119,123,1,6,123,127,1,127,5,131,2,9,131,135,1,5,135,139,1,139,10,143,1,143,2,147,1,147,5,0,99,2,0,14,0]
