module Day5 where

import Debug.Trace

input = [3,225,1,225,6,6,1100,1,238,225,104,0,1101,72,36,225,1101,87,26,225,2,144,13,224,101,-1872,224,224,4,224,102,8,223,223,1001,224,2,224,1,223,224,223,1102,66,61,225,1102,25,49,224,101,-1225,224,224,4,224,1002,223,8,223,1001,224,5,224,1,223,224,223,1101,35,77,224,101,-112,224,224,4,224,102,8,223,223,1001,224,2,224,1,223,224,223,1002,195,30,224,1001,224,-2550,224,4,224,1002,223,8,223,1001,224,1,224,1,224,223,223,1102,30,44,225,1102,24,21,225,1,170,117,224,101,-46,224,224,4,224,1002,223,8,223,101,5,224,224,1,224,223,223,1102,63,26,225,102,74,114,224,1001,224,-3256,224,4,224,102,8,223,223,1001,224,3,224,1,224,223,223,1101,58,22,225,101,13,17,224,101,-100,224,224,4,224,1002,223,8,223,101,6,224,224,1,224,223,223,1101,85,18,225,1001,44,7,224,101,-68,224,224,4,224,102,8,223,223,1001,224,5,224,1,223,224,223,4,223,99,0,0,0,677,0,0,0,0,0,0,0,0,0,0,0,1105,0,99999,1105,227,247,1105,1,99999,1005,227,99999,1005,0,256,1105,1,99999,1106,227,99999,1106,0,265,1105,1,99999,1006,0,99999,1006,227,274,1105,1,99999,1105,1,280,1105,1,99999,1,225,225,225,1101,294,0,0,105,1,0,1105,1,99999,1106,0,300,1105,1,99999,1,225,225,225,1101,314,0,0,106,0,0,1105,1,99999,7,677,226,224,102,2,223,223,1005,224,329,101,1,223,223,8,677,226,224,1002,223,2,223,1005,224,344,1001,223,1,223,1107,677,677,224,102,2,223,223,1005,224,359,1001,223,1,223,1107,226,677,224,102,2,223,223,1005,224,374,101,1,223,223,7,226,677,224,102,2,223,223,1005,224,389,101,1,223,223,8,226,677,224,1002,223,2,223,1005,224,404,101,1,223,223,1008,226,677,224,1002,223,2,223,1005,224,419,1001,223,1,223,107,677,677,224,102,2,223,223,1005,224,434,101,1,223,223,1108,677,226,224,1002,223,2,223,1006,224,449,101,1,223,223,1108,677,677,224,102,2,223,223,1006,224,464,101,1,223,223,1007,677,226,224,102,2,223,223,1006,224,479,101,1,223,223,1008,226,226,224,102,2,223,223,1006,224,494,101,1,223,223,108,226,226,224,1002,223,2,223,1006,224,509,101,1,223,223,107,226,226,224,102,2,223,223,1006,224,524,101,1,223,223,1107,677,226,224,102,2,223,223,1005,224,539,1001,223,1,223,108,226,677,224,1002,223,2,223,1005,224,554,101,1,223,223,1007,226,226,224,102,2,223,223,1005,224,569,101,1,223,223,8,226,226,224,102,2,223,223,1006,224,584,101,1,223,223,1008,677,677,224,1002,223,2,223,1005,224,599,1001,223,1,223,107,226,677,224,1002,223,2,223,1005,224,614,1001,223,1,223,1108,226,677,224,102,2,223,223,1006,224,629,101,1,223,223,7,677,677,224,1002,223,2,223,1005,224,644,1001,223,1,223,108,677,677,224,102,2,223,223,1005,224,659,101,1,223,223,1007,677,677,224,102,2,223,223,1006,224,674,101,1,223,223,4,223,99,226]

day5 = handleOppCode 0 input 1 []
day5' = handleOppCode 0 input 5 []

handleOppCode :: Int -> [Int] -> Int -> [Int] -> [Int]
handleOppCode index instructions input output
  | code oppCode == 1 =
    let newInstructions =
          replaceNth
            (instructions !! (index + 3))
            (extractValue (i1 oppCode) (index + 1) instructions + extractValue (i2 oppCode) (index + 2) instructions)
            instructions
     in handleOppCode (index + 4) newInstructions input output
  | code oppCode == 2 =
    let newInstructions =
          replaceNth
            (instructions !! (index + 3))
            (extractValue (i1 oppCode) (index + 1) instructions * extractValue (i2 oppCode) (index + 2) instructions)
            instructions
     in handleOppCode (index + 4) newInstructions input output
  | code oppCode == 3 =
    let newInstructions = replaceNth (instructions !! (index + 1)) input instructions
     in handleOppCode (index + 2) newInstructions input output
  | code oppCode == 4 =
    handleOppCode (index + 2) instructions input (output ++ [extractValue (i1 oppCode) (index + 1) instructions])
  | code oppCode == 5 =
    if extractValue (i1 oppCode) (index + 1) instructions /= 0
      then handleOppCode (extractValue (i2 oppCode) (index + 2) instructions) instructions input output
      else handleOppCode (index + 3) instructions input output
  | code oppCode == 6 =
      if extractValue (i1 oppCode) (index + 1) instructions == 0
        then handleOppCode (extractValue (i2 oppCode) (index + 2) instructions) instructions input output
        else handleOppCode (index + 3) instructions input output
  | code oppCode == 7 =
      let newInstructions =
            replaceNth
              (instructions !! (index + 3))
              (if (extractValue (i1 oppCode) (index + 1) instructions < extractValue (i2 oppCode) (index + 2) instructions) then 1 else 0)
              instructions
       in handleOppCode (index + 4) newInstructions input output
  | code oppCode == 8 =
        let newInstructions =
              replaceNth
                (instructions !! (index + 3))
                (if (extractValue (i1 oppCode) (index + 1) instructions == extractValue (i2 oppCode) (index + 2) instructions) then 1 else 0)
                instructions
         in handleOppCode (index + 4) newInstructions input output
  | code oppCode == 99 = output
  | otherwise = error ("not an existing oppCode: " ++ show (code oppCode))
  where
    oppCode = getOppCode (instructions !! index)

extractValue :: Int -> Int -> [Int] -> Int
extractValue 0 index a = a!!(a!!index)
extractValue 1 index a = a!!index

getOppCode :: Int -> OppCode
getOppCode i = OppCode (dt!!0) (dt!!1) (dt!!2) (dt!!3 * 10 + dt!!4)
  where
    ds = digits i
    dt = replicate (5 - length ds) 0 ++ ds

digits :: Int -> [Int]
digits = map (read . return) . show

data OppCode =
  OppCode
    { i3 :: Int
    , i2 :: Int
    , i1 :: Int
    , code :: Int
    }

instance Show OppCode where
  show (OppCode i1 i2 i3 code) = show code ++ "-" ++ show i1 ++ "," ++ show i2 ++ "," ++ show i3

replaceNth :: Int -> Int -> [Int] ->[Int]
replaceNth _ _ [] = []
replaceNth 0 newVal (x:xs) = newVal:xs
replaceNth n newVal (x:xs) = x:replaceNth (n-1) newVal xs