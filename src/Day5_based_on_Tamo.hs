module Day5_based_on_Tamo where

import Debug.Trace

program :: Program
program = [3,225,1,225,6,6,1100,1,238,225,104,0,1101,72,36,225,1101,87,26,225,2,144,13,224,101,-1872,224,224,4,224,102,8,223,223,1001,224,2,224,1,223,224,223,1102,66,61,225,1102,25,49,224,101,-1225,224,224,4,224,1002,223,8,223,1001,224,5,224,1,223,224,223,1101,35,77,224,101,-112,224,224,4,224,102,8,223,223,1001,224,2,224,1,223,224,223,1002,195,30,224,1001,224,-2550,224,4,224,1002,223,8,223,1001,224,1,224,1,224,223,223,1102,30,44,225,1102,24,21,225,1,170,117,224,101,-46,224,224,4,224,1002,223,8,223,101,5,224,224,1,224,223,223,1102,63,26,225,102,74,114,224,1001,224,-3256,224,4,224,102,8,223,223,1001,224,3,224,1,224,223,223,1101,58,22,225,101,13,17,224,101,-100,224,224,4,224,1002,223,8,223,101,6,224,224,1,224,223,223,1101,85,18,225,1001,44,7,224,101,-68,224,224,4,224,102,8,223,223,1001,224,5,224,1,223,224,223,4,223,99,0,0,0,677,0,0,0,0,0,0,0,0,0,0,0,1105,0,99999,1105,227,247,1105,1,99999,1005,227,99999,1005,0,256,1105,1,99999,1106,227,99999,1106,0,265,1105,1,99999,1006,0,99999,1006,227,274,1105,1,99999,1105,1,280,1105,1,99999,1,225,225,225,1101,294,0,0,105,1,0,1105,1,99999,1106,0,300,1105,1,99999,1,225,225,225,1101,314,0,0,106,0,0,1105,1,99999,7,677,226,224,102,2,223,223,1005,224,329,101,1,223,223,8,677,226,224,1002,223,2,223,1005,224,344,1001,223,1,223,1107,677,677,224,102,2,223,223,1005,224,359,1001,223,1,223,1107,226,677,224,102,2,223,223,1005,224,374,101,1,223,223,7,226,677,224,102,2,223,223,1005,224,389,101,1,223,223,8,226,677,224,1002,223,2,223,1005,224,404,101,1,223,223,1008,226,677,224,1002,223,2,223,1005,224,419,1001,223,1,223,107,677,677,224,102,2,223,223,1005,224,434,101,1,223,223,1108,677,226,224,1002,223,2,223,1006,224,449,101,1,223,223,1108,677,677,224,102,2,223,223,1006,224,464,101,1,223,223,1007,677,226,224,102,2,223,223,1006,224,479,101,1,223,223,1008,226,226,224,102,2,223,223,1006,224,494,101,1,223,223,108,226,226,224,1002,223,2,223,1006,224,509,101,1,223,223,107,226,226,224,102,2,223,223,1006,224,524,101,1,223,223,1107,677,226,224,102,2,223,223,1005,224,539,1001,223,1,223,108,226,677,224,1002,223,2,223,1005,224,554,101,1,223,223,1007,226,226,224,102,2,223,223,1005,224,569,101,1,223,223,8,226,226,224,102,2,223,223,1006,224,584,101,1,223,223,1008,677,677,224,1002,223,2,223,1005,224,599,1001,223,1,223,107,226,677,224,1002,223,2,223,1005,224,614,1001,223,1,223,1108,226,677,224,102,2,223,223,1006,224,629,101,1,223,223,7,677,677,224,1002,223,2,223,1005,224,644,1001,223,1,223,108,677,677,224,102,2,223,223,1005,224,659,101,1,223,223,1007,677,677,224,102,2,223,223,1006,224,674,101,1,223,223,4,223,99,226]

type Program = [Int]
type Inputs = [Int]
type Position = Int
type Value = Int
type Output = (Program, [Value])
type Opcode = Int
type ParametersImmediate = [Bool]
type Operation = (Opcode, ParametersImmediate, Int) -- how many output parameters

day5 = output
      where (prog, output) = run 0 program [1]

run :: Position -> Program -> Inputs -> Output
run p prog inputs
  | opCode == 1 = cont (writeOutput (sum inputParameters)) inputs
  | opCode == 2 = cont (writeOutput (product inputParameters)) inputs
  | opCode == 3 = cont (writeOutput (head inputs)) (tail inputs)
  | opCode == 4 = cont prog (inputs ++ inputParameters)
  | opCode == 99 = (prog, inputs)
  | otherwise = error ("opCo de " ++ show opCode ++ " on position " ++ show p ++ " is not mapped --> " ++ show prog)
  where
    (opCode, inputModes, output) = parseOperation (prog !! p)
    outputPos = prog !! (p + length inputModes + 1)
    writeOutput x = changeProgram x outputPos prog
    totalNrParams = length inputModes + output
    inputParameters = readInputParameters inputModes (p + 1) prog
    cont = run (p + totalNrParams + 1)

readInputParameters :: ParametersImmediate -> Position -> Program -> [Value]
readInputParameters [] _ _ = []
readInputParameters (True:xs) pos prog = (prog !! pos) : readInputParameters xs (pos + 1) prog
readInputParameters (False:xs) pos prog = (prog !! (prog !! pos)) : readInputParameters xs (pos + 1) prog

changeProgram :: Int -> Int -> Program -> Program
changeProgram v 0 (_ : tail) = v : tail
changeProgram v p (h:tail) = h : changeProgram v (p - 1) tail

parseOperation :: Int -> Operation
parseOperation i = (opcode, take amountOfInputParameters (parseParameterModes (div i 100)), amountOfOutputParameters)
  where
    parseParameterModes 0 = repeat False
    parseParameterModes x = (x `mod` 10 == 1) : parseParameterModes (x `div` 10)
    amountOfInputParameters
      | opcode == 1 = 2
      | opcode == 2 = 2
      | opcode == 3 = 0
      | opcode == 4 = 1
      | opcode == 5 = 2
      | opcode == 6 = 2
      | opcode == 7 = 2
      | opcode == 8 = 2
      | opcode == 99 = 0
    amountOfOutputParameters
      | opcode == 1 = 1
      | opcode == 2 = 1
      | opcode == 3 = 1
      | opcode == 4 = 0
      | opcode == 5 = 0
      | opcode == 6 = 0
      | opcode == 7 = 1
      | opcode == 8 = 1
      | opcode == 99 = 0
    opcode = (mod i 100)