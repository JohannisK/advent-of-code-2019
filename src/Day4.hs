module Day4 where

import Data.List
import Data.Function

input = [138307..654504]

day4 = length (filter containsDouble (filter notDecreasing [digits x | x <- input]))
day4' = length (filter containsExactDouble (filter notDecreasing [digits x | x <- input]))

digits :: Integer -> [Int]
digits = map (read . return) . show

notDecreasing :: [Int] -> Bool
notDecreasing (x:[]) = True
notDecreasing (x:xs:rest)
  | x <= xs = notDecreasing (xs : rest)
  | otherwise = False

containsDouble :: [Int] -> Bool
containsDouble (x:[]) = False
containsDouble (x:xs:rest)
  | x == xs = True
  | otherwise = containsDouble (xs : rest)

containsExactDouble :: [Int] -> Bool
containsExactDouble list = 1 `elem` [ length x | x <- groupBy (\(a,b) (c,d) -> a == c && b == d) (filter (\(a,b) -> a == b) (zip list (tail list))) ]
